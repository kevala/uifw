import React from 'react'
import Menu, {SubMenu, MenuItem} from 'rc-menu'

export const MenuBar = (props) => (
  <Menu mode="horizontal">
    <SubMenu title="Programs"></SubMenu>
    <SubMenu title="Trainees"></SubMenu>
    <SubMenu title="Teachers"></SubMenu>
    <SubMenu title="Reports"></SubMenu>
    <SubMenu title="Masters">
      <MenuItem>Program Types</MenuItem>
      <MenuItem>Uniform Sizes</MenuItem>
      <MenuItem>Contact Tags</MenuItem>
      <MenuItem>Program Categories</MenuItem>
      <MenuItem>School Years</MenuItem>
      <MenuItem>Asset Types</MenuItem>
    </SubMenu>
  </Menu>
)
