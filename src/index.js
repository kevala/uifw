import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import { routerForBrowser, initializeCurrentLocation } from 'redux-little-router'
import { composeWithDevTools } from 'redux-devtools-extension'
import App from './App'
import rootReducer from './reducers'
import './css.js'
import routes from './routes'

const { reducer, middleware, enhancer } = routerForBrowser({
  routes
})

const store = createStore(
  combineReducers({ router: reducer }),
  {},
  composeWithDevTools(enhancer, applyMiddleware(middleware))
)

const initialLocation = store.getState().router;
if (initialLocation) {
  store.dispatch(initializeCurrentLocation(initialLocation));
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
