import React, { Component } from 'react'
import { MenuBar } from './layout/MenuBar.js'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <MenuBar/>
      </div>
    );
  }
}

export default App;
